#include "sierrachart.h"

SCDLLName("BuyStrategy")

/*==========================================================================*/
SCSFExport scsf_BuyStrategy(SCStudyInterfaceRef sc)
{
	SCSubgraphRef Buy = sc.Subgraph[0];
	SCSubgraphRef ExitBuy = sc.Subgraph[1];
	
	SCInputRef ArrowOffsetPercentage = sc.Input[0];
	SCInputRef Quantity = sc.Input[1];
	
	if (sc.SetDefaults)
	{
        sc.GraphName = "Buy strategy"; // Set the configuration and defaults
        sc.StudyDescription = "This strategy only buys and exit the position.";
		sc.FreeDLL = 1;	// During development set this flag to 1, so the DLL can be rebuilt without restarting Sierra Chart. When development is completed, set it to 0 to improve performance.	
        sc.AutoLoop = 1; // Setting sc.AutoLoop to 1 (true) means looping is performed automatically. This means that if there are 100 bars in your chart, this function is called 100 times initially.
		sc.GraphRegion = 0; //Main chart region
		sc.MaximumPositionAllowed = 100000000;//use a high-value to effectively not put any Position Quantity restrictions
		sc.SendOrdersToTradeService = true;
		
		Buy.Name = "Buy";
		Buy.PrimaryColor = RGB(0, 0, 255);	// blue
		Buy.DrawStyle = DRAWSTYLE_ARROWUP;
		Buy.LineWidth = 2;	//Width of arrow
		
		ExitBuy.Name = "ExitBuy";
		ExitBuy.DrawStyle = DRAWSTYLE_ARROWDOWN;
		ExitBuy.PrimaryColor = RGB(255, 255, 255);	// white
		ExitBuy.LineWidth = 2; //Width of arrow
		
		ArrowOffsetPercentage.Name = "Arrow Offset Percentage";
		ArrowOffsetPercentage.SetInt(5);
		
		Quantity.Name = "Order quantity";
		Quantity.SetInt(233);
		
		return;
	}
	
	int Index=sc.Index; //Sets the variable Index to the current Bar index.
	
	/* This is used further down to position a buy or ExitBuy arrow below or above a price bar so it does not touch exactly the low or high.  This is not necessary.  It is only for appearance purposes.*/
	float Offset = (sc.High[Index] -sc.Low[Index] )*(ArrowOffsetPercentage.GetInt() * 0.01f);

	// Array references
	SCFloatArrayRef High = sc.High;
	SCFloatArrayRef Low = sc.Low;	
		
	// Do data processing
	if (High[sc.Index] > High[sc.Index - 1] && High[sc.Index] > High[sc.Index - 2]) 
	{
		//Place an Up arrow below the low of the current bar. 
		Buy[Index] = sc.Low[Index] - Offset;
		
		// Create a market order and enter long.
        s_SCNewOrder order;
        order.OrderQuantity = Quantity.GetInt();
        order.OrderType = SCT_ORDERTYPE_MARKET;

        sc.BuyEntry(order);	
	}
	
	if (Low[sc.Index] < Low[sc.Index - 1] && Low[sc.Index] < Low[sc.Index - 2])
	{
		ExitBuy[Index] = sc.High[Index] + Offset;
		
		// Create a market order and enter long.
        s_SCNewOrder order;
        order.OrderQuantity = Quantity.GetInt();
        order.OrderType = SCT_ORDERTYPE_MARKET;

        sc.BuyExit(order);	
	} 
}
 
