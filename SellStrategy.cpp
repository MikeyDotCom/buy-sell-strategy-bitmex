#include "sierrachart.h"

SCDLLName("SellStrategy")

/*==========================================================================*/
SCSFExport scsf_SellStrategy(SCStudyInterfaceRef sc)
{
	SCSubgraphRef Sell = sc.Subgraph[0];
	SCSubgraphRef ExitSell = sc.Subgraph[1];
	
	SCInputRef ArrowOffsetPercentage = sc.Input[0];
	SCInputRef Quantity = sc.Input[1];
	
	if (sc.SetDefaults)
	{
        sc.GraphName = "Sell strategy"; // Set the configuration and defaults
        sc.StudyDescription = "This strategy only sells and exit the position";
		sc.FreeDLL = 1;	// During development set this flag to 1, so the DLL can be rebuilt without restarting Sierra Chart. When development is completed, set it to 0 to improve performance.	
        sc.AutoLoop = 1; // Setting sc.AutoLoop to 1 (true) means looping is performed automatically. This means that if there are 100 bars in your chart, this function is called 100 times initially.
		sc.GraphRegion = 0; //Main chart region
		sc.MaximumPositionAllowed = 100000000;//use a high-value to effectively not put any Position Quantity restrictions
		sc.SendOrdersToTradeService = TRUE;
		
		Sell.Name = "Sell";
		Sell.PrimaryColor = RGB(255, 255, 255);	// white
		Sell.DrawStyle = DRAWSTYLE_ARROWUP;
		Sell.LineWidth = 2;	//Width of arrow
		
		ExitSell.Name = "ExitSell";
		ExitSell.DrawStyle = DRAWSTYLE_ARROWDOWN;
		ExitSell.PrimaryColor = RGB(255, 0, 0);	// red
		ExitSell.LineWidth = 2; //Width of arrow
		
		ArrowOffsetPercentage.Name = "Arrow Offset Percentage";
		ArrowOffsetPercentage.SetInt(5);
		
		Quantity.Name = "Order quantity";
		Quantity.SetInt(500);
		
		return;
	}
	
	int Index=sc.Index; //Sets the variable Index to the current Bar index.
	
	/* This is used further down to position a Sell or ExitSell arrow below or above a price bar so it does not touch exactly the low or high.  This is not necessary.  It is only for appearance purposes.*/
	float Offset=(sc.High[Index] -sc.Low[Index] )*(ArrowOffsetPercentage.GetInt() * 0.01f);
	
	// Array references
	SCFloatArrayRef High = sc.High;
	SCFloatArrayRef Low = sc.Low;	
	
	// Do data processing
	if ( High[sc.Index] > High[sc.Index - 1] && High[sc.Index] > High[sc.Index - 2])
	{
		//Place an Up arrow below the low of the current bar. 
		Sell[Index] = sc.Low[Index] - Offset;

		/*Make sure we have no down arrow from a prior calculation.  It is important to set this to zero since we perform calculations on the same bar many times while the bar is being updated and we may have had a prior sell signal when we now have a buy signal or no signal.*/
		ExitSell[Index] = 0;
		
		// Create a market order and exit Short.
        s_SCNewOrder order;
        order.OrderQuantity = Quantity.GetInt();
        order.OrderType = SCT_ORDERTYPE_MARKET;

        sc.SellExit(order);	
	
	}
	else if ( Low[sc.Index] < Low[sc.Index - 1] && Low[sc.Index] < Low[sc.Index - 2])
	{
		ExitSell[Index] = sc.High[Index] + Offset;
		Sell[Index] = 0;		

		// Create a market order and enter Short.
        s_SCNewOrder order;
        order.OrderQuantity = Quantity.GetInt();
        order.OrderType = SCT_ORDERTYPE_MARKET;

        sc.SellEntry(order);
	} 
	else
	{
		Sell[Index] = 0;
		ExitSell[Index] = 0;
	}
}
 
